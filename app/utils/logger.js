'use strict';


const cli = require('bart96-style-cli');
const Format = require('bart96-format');


class Logger {
  constructor() {
    this.format = new Format();
    this.config = {
      debugMode : false,
      formatDate: '[yyyy.mm.dd HH:MM]',
      typeFormat: 'error',
      typeColors: {
        'Null'  : 'white',

        'error' : 'red',
        'warn'	: 'yellow',
        'info'	: 'blue',

        'cmd'		: 'magenta',
        'bot'		: 'cyan',
        'user'  : 'cyan',
        'memory': 'green',
      }
    };
  }

  _getType(type) {
    if (typeof type == 'number') return Object.keys(this.config.typeColors)[type] || 'NotFound';
    if (typeof type == 'string') return this.config.typeColors[type] ? type : 'NotFound';
    return 'Null';
  }

  log(moduleParent, args) {
    let date = this.format.date(new Date(), this.config.formatDate);
    let type = this.config.typeFormat;
    let exit = false;

    let msg = '';
    let user = '';
    let color = '';

    for(let el of args) {
      if (!el) return;

      switch (typeof el) {
        case 'string': msg += ` ${el}`; break;
        case 'number': type = Math.abs(el); if (el < 0) exit = true; break;
  			case 'object':
  				if (Array.isArray(el) && el[0].stack) msg += this.config.debugMode ? el[0].stack : el[1];
  				else if (el.stack && el.name && el.message) msg += this.config.debugMode ? el.stack : `${msg?' \u27A4':''} ${el.name}: ${el.message}`;
  				else if (el.author) {user = el.author.tag.replace(/#(\d{4})$/, ' $1'); type=6}
  				else this.log(module, [`Отсутствует условие для объекта "${el}". Элемент пропущен`, 2]);
  			break;
        // case 'function': ; break;

        default: this.log(module, [`Неизвестный формат "${typeof el}" у "${el}"`, 2]);
      }
    }

    if (!msg) return;

    user = this._getType(type);
    color = this.config.typeColors[user] || this.config.typeColors['Null'];

    msg = msg
      .replace(/\s(?:Warn|Error)/g, a => cli[a == 'Warn' ? 'yellow' : 'red'](' '+a))
      .replace(/(✖|✔)/ig, a => cli[a == '✔' ? 'green' : 'red'](a))
      .replace(/[\w\d]{24}\.[\w\d]{6}\.[\w\d-_]{27}/g, '[ClientToken]')
      .replace('  ', ' ');

    msg = cli`{${color} > ${date}} [{${color}Bright ${user}}]{white \t=>} (${moduleParent.filename.split(/\/|\\/).pop()}){blackBright ${msg}}`;

    exit ? process.exit(console.log(msg)) : console.log(msg);
  }
}



module.exports = Logger;
