'use strict';


const nodemailer = require('nodemailer');

const container = require('./container.js');


class Mail {
  constructor(address) {
    this.config = container.get('config');
    this.logger = container.get('logger');
    this.log = (...args) => this.logger.log(module, args);

    this.mailData = this.check(address || 'smtpRambler');
    this.transport = nodemailer.createTransport(mailData);
  }

  check(address) {
    if (this.config.mail[address]) return this.config.mail[address];
    else this.log(`Отсутствуют данные почты с адресом "${address}".`, -1);
  }

  send(mail) {
    mail = {
      from: `ProShop администрация <${this.mailData.auth.user}>`,
      to: 'igor96@rambler.ru',
      subject: 'Проверка',
      text: 'Node.js ♥',
      html: '<b>Node.js ♥ you</b>',
    };

    this.transport.sendMail(mail, (err, response) => {
      if (err) this.log(err);
      else this.log(`Письмо получил пользователь: ${response.accepted.join(',')}`, 3);
      this.transport.close();
    });
  }

}



module.exports = Mail;
