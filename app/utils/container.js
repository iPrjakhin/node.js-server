'use strict';

const path = require('path');
const {readdir} = require('fs');

const Logger = require('./logger.js');


class Container {
  constructor() {
    this.log = (...args) => (new Logger()).log(module, args);

    this._map = new Map();
    this._name = module.filename.split(/\/|\\/).pop();
  }


  _set(path, saveThis) {return this._map.set(path, saveThis);}
  _get(path) {return this._map.get(path) || null;}

  get(path) {
    let keys = path.split(':').reverse();
      if (keys.length == 1) return this._get(path);

    return keys.reduce((json, key) => {
      delete json._pathSize;
      return json[key];
    }, this._map.get(keys.pop()));
  }

  set(path, saveThis) {
    let keys = path.split(':').reverse();
      if (keys.length == 1) return this._set(path, saveThis);

    let categoryName = keys.pop();
    let category = this._get(categoryName);
      // if (!category) return this._map.set(categoryName, Object.assign(this._createJSON(keys, saveThis), {'_pathSize':keys.length}));
      if (!category) return this._map.set(categoryName, this._createJSON(keys, saveThis));

    // let pathSize = category._pathSize;
    //   if (!pathSize) throw new Error(`Ключ "${categoryName}" существует, но не является категорией.`);
    //   if (keys.length > pathSize) throw new Error(`Категория "${categoryName}" существует, но не запрашиваемый уровень.`);

    return this._map.set(categoryName, this._mergeDeep(category, this._createJSON(keys, saveThis)));
  }


  _createJSON(keys, saveThis) {
    return keys.reduce((json, key) => {
    	if (Object.keys(json).length == 0) {
    		json[key] = saveThis;
    		return json;
      }

    	return Object.create(Object.create(null), {[key]: {value: json, configurable: true, writable: true, enumerable: true}});
    }, Object.create(null));
  }


  _isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item));
  }

  _mergeDeep(target, source) {
    let output = Object.assign({}, target);
    if (this._isObject(target) && this._isObject(source)) {
      Object.keys(source).forEach(key => {
        if (this._isObject(source[key])) {
          if (!(key in target)) Object.assign(output, {[key]: source[key]});
          else output[key] = this._mergeDeep(target[key], source[key]);
        }
        else Object.assign(output, {[key]: source[key]});
      });
    }
    return output;
  }

  getAllFiles(dirname = './', opt, callback = ()=>{}) {
    opt = Object.assign({blackList:[], folder:true, save:true}, opt);

    let url;
    if (url = module.parent.filename.match(/(.*)(?:\/|\\)\w+\.js$/)) {
      let dir = path.resolve(url[1], dirname);

      /* // >>> ReaddirSync <<<

        try {
          return readdirSync(dir).filter(f => (!~opt.blackList.indexOf(f) && /^[^_].*\.js$/i.test(f))).map(f => {
            let fileName = f.split('.')[0];
            let file = require(path.resolve(dir, f));

            this.log(`✔ ${f}`, 3);

            if (opt.save) this.set((opt.folder ? dir.split(/\/|\\/).pop() +':' : '') + fileName, file.name ? new file() : file);
            else return file.name ? new file() : file;
          });
        } catch(err) {this.log(err)}
      */


      return new Promise((resolve, reject) => {
        readdir(dir, (err, files) => {
          this.log(`Loading all files from "${dir}".`, 3);
          if (err) return reject(err);

          let filesArr = [];

          files.filter(f => (!~opt.blackList.indexOf(f) && /^[^_].*\.js$/i.test(f))).map(f => {
            try {
              let fileName = f.split('.')[0];
              let file = require(path.resolve(dir, f));
                  file = file.name && file.name != 'router' ? new file() : file;

              if (opt.save) this.set((opt.folder ? dir.split(/\/|\\/).pop() +':' : '') + fileName, file);
              else {
                switch (opt.type) {
                  case 'arr': filesArr.push(file);  break;
                  case 'obj': filesArr.push({name:fileName, file:file});  break;

                  default: this.log(`Отсутствует условие для opt.type="${opt.type}".`, 2);
                }
              }

              this.log(`✔ ${f}`, 3);
            } catch(err) {this.log(`✖ ${f} `, err)}
          });

          resolve(filesArr);
        });
      });

    }
  }

}



/**
// @version 1.0.0
// @description Простейшая версия с возможностью создавать уникальные имена

class Container {
  constructor() {
    this._map = new Map();
  }

  set(name, newClass, replaceable=false) {
    if (this._map.has(name) && this._map.get(name).replaceable)
      throw new Error(`Ключ "${name}" уже существует и является уникальным.`);

    this._map.set(name, {definition:newClass, replaceable:replaceable});
  }

  get(name) {
    return (this._map.get(name) || '').definition;
  }
}
**/



const container = new Container();
module.exports = container;
