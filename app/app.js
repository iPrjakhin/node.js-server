/* Автор - Пряхин Игорь  ::  BART96  ::  Author - Prjakhin Igor */
/* Уважайте чужой труд.  ::  Respect other peoples work. */

'use strict';


const fs = require('fs');
const path = require('path');
// const url = require('url');

const async = require('async');
const redis = require('redis');
const mysql = require('mysql');
const express = require('express');
const session = require('express-session');
// const subDomain = require('express-subdomain');
const SessRed = require('connect-redis')(session);
const rewrite = require('connect-modrewrite');
const hbs     = require('express-handlebars');

const Format = require('bart96-format');

const Config = require('./config.js');
const Logger = require('./utils/logger.js');
const container = require('./utils/container.js');


class App {
  constructor() {
    this.package = require('../package.json');
    this.appName = this.package.name[0].toUpperCase() + this.package.name.slice(1);

    container.set('format', new Format());
    this.format = container.get('format');

    container.set('config', new Config());
    this.config = container.get('config');

    container.set('logger', new Logger());
    this.logger = container.get('logger');
    this.log = (...args) => this.logger.log(module, args);
    this.log(`{white Starting ${this.appName} v${this.package.version}}`, 3);

    container.set('sessRedis', session(Object.assign(
      {store: new SessRed(this.config.session.redis)},
      this.config.redis.options,
      this.config.session.options,
    )));

    this.init();
  }


  init() {
    this.log('{white Initialising components...}', 3);

    let app = express();

    async.parallel({
      start: callback => {
        app.listen(this.config.port, this.config.ip || '', () =>
          callback(null, this.log(`Сервер ${this.config.baseUrl} успешно запущен на порту: ${this.config.port}`, 3))
        ).on('error', err => callback(err));
      },

      redis: callback => {
        let redisClient = redis.createClient(this.config.redis.options);
        redisClient.select(this.config.redis.select || 0);

        container.set('redis', redisClient);
        this.redis = container.get('redis')
          .on('error',   err => this.log(err, -7))
          .on('warning', err => this.log(err, 7))
          .on('ready',    () => callback(null, this.log('Redis ready', 7)));
      },

      mysql: callback => {
        this.mysql = mysql.createConnection(this.config.mysql.options);
        this.mysql.connect(err => err ? this.log(err, -7) : callback(null, this.log('MySQL ready', 7)));
        container.set('mysql', this.mysql);
      },

      template: callback => {
        app.engine('hbs', hbs.create({
          layoutsDir: path.join(__dirname, 'views/layouts'),
          partialsDir: path.join(__dirname, 'views/partials'),
          defaultLayout: 'layout',
          extname: 'hbs',
          helpers: {
            'toString': obj => JSON.stringify(obj),
            'include': template => template.data.root['page'],
            'ifType': (row, head) =>
              !head.type ? row[head.key]
              : head.type == 'date' ? this.format.date(row[head.key] < 1e12 ? row[head.key]*1000 : row[head.key])
              : head.type == 'bool' ? `<i uk-icon="${row[head.key] == true ? 'check' : ''}"></i>`
              : head.type == 'text' ? row[head.key].replace(/\r\n/g, '<br>')
              : head.type == 'url' ? `<a href="${row[head.key]}" target="_blank">${row[head.key]}</a>`
              : row[head.key],
            'slice': (str, start, end) => (end ? str.slice(start, end) : str.slice(start)) + '...',
            'math': (lvalue, operator, rvalue) => {
              lvalue = parseFloat(lvalue);
              rvalue = parseFloat(rvalue);
              return {
                  '+': lvalue + rvalue,
                  '-': lvalue - rvalue,
                  '*': lvalue * rvalue,
                  '/': lvalue / rvalue,
                  '%': lvalue % rvalue,
              }[operator];
            },
          }
        }).engine);
        app.set('view engine', '.hbs');
        app.set('views', path.join(__dirname, 'views'));

        callback(null);
      },

      middleware: callback => {
        app.use(express.static(path.resolve(__dirname, 'public')));
        app.use(express.urlencoded({extended:true}));
        app.use(express.json());

        app.use(rewrite(
          this.config.rewrite.map(arr => `${arr[0]} ${arr[1] + (arr[2] ? ` [${arr[2] + (arr[3] ? `=${arr[3]}`:'')}]`:'')}`)
        ));

        // app.use((req, res, next) => {console.log(req.url); next();});

        container.getAllFiles('./middleware/', {save:false, type:'arr'}).then(resolve => {
          for(let middleware of resolve) app.use((req, res, next) => middleware(req, res, next, app));
          callback(null);
        }).catch(err => callback(err));

        // app.use((req, res, next) => {console.log(req.url); next();});
      },

      route: callback => {
        container.getAllFiles('./routes/', {save:false, type:'obj'}).then(resolve => {
          for(let route of resolve) app.use('/'+ route.name, route.file);
          callback(null);
        }).catch(err => callback(err));
      },

    }, (err, results) => {
      if (err) return this.log(err, -1);

      app.use((req, res, next) => {
        res.status(404);
        res.render('error', {code:404});
      });

      app.use((err, req, res, next) => {
        res.status(err.code || 500);

        if (typeof err == 'object' && typeof err.code == 'number')
          res.render('error', {code:err.code, text:err.message||''});
        else {
          this.log(err);
          res.render('error', {code:500, text:'Непредвиденная ошибка'});
        }
      });

      this.testing();
    });
  }


  testing() {
    async.waterfall([
      // >>> MySQL -> dbExists <<<
      next => this.mysql.query(`SHOW DATABASES LIKE '${this.config.mysql.select}'`, (err, rows) => next(err, rows[0])),
      (rows, next) => rows ? next(null) : this.mysql.query(`CREATE DATABASE ${this.config.mysql.select}`, (err, rows) => next(err)),
      next => this.mysql.query(`USE ${this.config.mysql.select}`, (err, rows) => next(err)),

      // >>> MySQL -> tableExists <<<
      next => {
        let query = '';
        let tables = this.config.mysql.tables;

        for (let tableName in tables)
          query += `CREATE TABLE IF NOT EXISTS ${tables[tableName].name} (${tables[tableName].fields.join(',')}) CHARSET=utf8 COLLATE utf8_general_ci;`;

        this.mysql.query(query, (err, rows) => next(err));
      },

    ], (err, results) => err ? this.log(err, -1) : this.finish());
  }


  finish() {
    this.log('{white Сompleted successfully\n}', 3);
  }

}



module.exports = App;
