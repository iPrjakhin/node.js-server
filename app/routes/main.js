'use strict';

const container = require('../utils/container.js');
let config = container.get('config');
let mysql = container.get('mysql');

const express = require('express');
const router = express.Router()
  .get('/', (req, res) => res.render('main', {page:'main/main'}))
  .get('/info', (req, res) => res.render('main', {page:'main/info'}))
  .get('/shop', (req, res) => {
    mysql.query(`SELECT name,image,feature from ${config.mysql.tables.goods.name}`, (err, rows) => {
      if (err) return log(err);
      res.render('main', {page:'main/shop', goods: rows});
    });
  })

  .get('/contacts', (req, res) => res.render('main', {page:'main/contacts'}))

  .get('*', (req, res) => res.render('error', {code:'404'}));



module.exports = router;




// 'use strict';
//
// const path = require('path');
// const express = require('express');
// const router = express.Router();
// const hbs = require('express-handlebars');
//
// module.exports = (req, res, next, app) => {
//   // app.engine('hbs', hbs.create({
//   //   // layoutsDir: path.join(__dirname, 'views/layouts'),
//   //   partialsDir: path.join(__dirname, 'views/main/partials'),
//   //   // defaultLayout: 'layout',
//   //   extname: 'hbs',
//   //   helpers: {
//   //     'include': template => template.data.root['page']
//   //   }
//   // }).engine);
//   app.set('views', path.join('/views/main'));
//
//
//   router
//     .get('/', (req, res) => res.render('general', {page:'main'}))
//     .get('/info', (req, res) => res.render('general', {page:'info'}))
//     .get('/shop', (req, res) => res.render('general', {page:'shop'}))
//     .get('/contacts', (req, res) => res.render('general', {page:'contacts'}))
//
//     .get('*', (req, res) => res.render('error', {code:'404123'}));
//
//   return router(req, res, next);
// };
