'use strict';


const express = require('express');
const router = express.Router()
  .get('/main', (req, res) => res.send('<h1>Я есть API!</h1>'))

  .get('*', (req, res) => res.render('error', {code:'404'}));



module.exports = router;
