const container = require('../../../utils/container.js');

module.exports = method => (req, res, next) => {
  let logger = container.get('logger');
  let log = (...args) => logger.log(module, args);

  let redis = container.get('redis');
  let mysql = container.get('mysql');


  redis.hgetall(`queryLogs:${method}`, (err, reply) => {
    if (err) {
      log(err);
      return res.render('error', {code:'500', text:'Внутренняя ошибка при запросе'});
    }

    let body = [];

    for(let date in reply) {
      let row = JSON.parse(reply[date]);
      row.date = date;

      body.push(row);
    }

    res.render('admin', {page:'admin/table', table: {
      name: method +'-запросов',
      head: [
        {key:'date', name:'Время запроса', type:'date'},
        {key:'ip', name:'IP-адрес'},
        {key:'url', name:'URL-адрес', type:'url'},
      ],
      body: body,
    }});

  });
};
