const container = require('../../../utils/container.js');

module.exports = (req, res, next) => {
  let logger = container.get('logger');
  let log = (...args) => logger.log(module, args);

  let redis = container.get('redis');
  let mysql = container.get('mysql');


  let login = req.body.login;
  let pass = req.body.pass;

  mysql.query('SHOW DATABASES', (err, rows) => {
    if (err) {
      log(err);
      res.render('error', {code:'500', text:'Внутренняя ошибка при запросе'});
    }

    if (login == '123') {
      req.session.adminAccess = {
        ip: req.ip,
        date: Date.now(),
        user: login,
      };
    }

    next();
  });

};
