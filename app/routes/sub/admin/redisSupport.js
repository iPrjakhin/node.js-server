const container = require('../../../utils/container.js');

module.exports = (req, res, next) => {
  let logger = container.get('logger');
  let log = (...args) => logger.log(module, args);

  let redis = container.get('redis');
  let mysql = container.get('mysql');


  redis.hgetall('messages', (err, reply) => {
    if (err) {
      log(err);
      return res.render('error', {code:'500', text:'Внутренняя ошибка при запросе'});
    }


    let body = [];

    for(let date in reply) {
      let row = JSON.parse(reply[date]);
      row.date = date;
      row.type = ['Приобретение', 'Доставка', 'Ремонт / настройка', 'Другое',][row.type];

      body.push(row);
    }

    res.render('admin', {page:'admin/table', table: {
      head: [
        {key:'date', name:'Время запроса', type:'date'},
        {key:'name', name:'Имя клиента'},
        {key:'mail', name:'Почтиа клиента'},
        {key:'type', name:'Категория'},
        {key:'message', name:'Доп.информация'},
      ],
      body: body,
    }});
  });

};
