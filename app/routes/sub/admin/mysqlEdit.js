const container = require('../../../utils/container.js');

module.exports = table => (req, res, next) => {
  let config = container.get('config');
  let logger = container.get('logger');
  let log = (...args) => logger.log(module, args);

  let redis = container.get('redis');
  let mysql = container.get('mysql');

  let users = config.mysql.tables['users'].name;
  let goods = config.mysql.tables['goods'].name;
  let mails = config.mysql.tables['mails'].name;
  let trans = config.mysql.tables['trans'].name;
  let bans = config.mysql.tables['bans'].name;

  let database = {
    users: {
      query: `
        SELECT name,email,UNIX_TIMESTAMP(regDate) AS 'regDate',UNIX_TIMESTAMP(dateStart) AS 'dateStart',UNIX_TIMESTAMP(dateEnd) AS 'dateEnd',reason
        FROM ${users}
        LEFT OUTER JOIN ${bans}
        ON ${users}.id = ${bans}.userID
      `,
      id: 'users',
      name: 'пользователей',
      head: [
        {key:'name', name:'Имя пользователя'},
        {key:'email', name:'Почта пользователя'},
        {key:'regDate', name:'Дата регистрации', type:'date', noForm:true},
        {key:'banned', name:'Блокировка', type:'bool', noForm:true},
      ],
      add: true,
      modal: true,
    },

    goods: {
      query: `
        SELECT type,name,price,amount,image,feature
        FROM ${goods}
      `,
      id: 'goods',
      name: 'товаров',
      head: [
        {key:'name', name:'Название товара'},
        {key:'type', name:'Группа товара'},
        {key:'price', name:'Цена товара'},
        {key:'amount', name:'Осталось в наличие'},
      ],
      add: true,
      modal: true,
    },

    mails: {
      query: `
        SELECT title,text
        FROM ${mails}
      `,
      id: 'mails',
      name: 'шаблонов писем',
      head: [
        {key:'title', name:'Заголовок письма'},
        {key:'text', name:'Тело письма', type:'text'},
      ],
      add: true,
      modal: true,
    },

    trans: {
      query: `
        SELECT
          UNIX_TIMESTAMP(date) AS 'date',
          CONCAT(${users}.name, ' <span class="counter" uk-tooltip="ID клиента">(<i>', ${users}.id, '</i>)</span>') AS 'userName',
          CONCAT(${goods}.name, ' <span class="counter" uk-tooltip="ID товара" >(<i>', ${goods}.id, '</i>)</span>') AS 'goodName',
          ${goods}.type AS 'goodType'
        FROM ${trans}
        LEFT JOIN ${users} ON ${trans}.userID = ${users}.id
        LEFT JOIN ${goods} ON ${trans}.goodID = ${goods}.id
      `,
      head: [
        {key:'date', name:'Дата транзакции', type:'date'},
        {key:'userName', name:'Клиент'},
        {key:'goodName', name:'Товар'},
        {key:'goodType', name:'Группа товара'},
      ]
    },
  }


  mysql.query(database[table].query, (err, rows, fields) => {
    if (err) {
      log(err);
      return res.render('error', {code:'500', text:'Внутренняя ошибка при запросе'});
    }

    res.render('admin', {
      page:'admin/table',
      data: {
        modal: database[table].modal,
        add: (() => {
          if (database[table].add == true) {
            let form = [];

            database[table].head.forEach(json => {
              if (json.noForm) return;

              form.push(
                json.type == 'text' ? `<textarea class="uk-textarea" rows="3" name="${json.key}" placeholder="${json.name}"></textarea>`
                : `<input type="text" name="${json.key}" class="uk-input" placeholder="${json.name}" />`
              );
            });

            return form;
          }
        })()
      },
      table: {
        id: database[table].id,
        name: database[table].name,
        head: database[table].head,
        body: rows,
      },
    });

  });
};
