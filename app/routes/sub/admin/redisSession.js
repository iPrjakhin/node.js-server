const container = require('../../../utils/container.js');

module.exports = (req, res, next) => {
  let logger = container.get('logger');
  let log = (...args) => logger.log(module, args);

  let redis = container.get('redis');
  let mysql = container.get('mysql');


  redis.keys('proShop:session:*', (err, reply) => {
    if (err) {
      log(err);
      return res.render('error', {code:'500', text:'Внутренняя ошибка при запросе'});
    }

    redis.mget(reply.map(str => str.replace(/^proShop\:/, '')), (err, reply) => {
      if (err) {
        log(err);
        return res.render('error', {code:'500', text:'Внутренняя ошибка при запросе'});
      }

      res.render('admin', {page:'admin/table', table: {
        head: [
          {key:'user', name:'Администратор'},
          {key:'date', name:'Время входа', type:'date'},
          {key:'ip', name:'IP-адрес'},
        ],
        body: reply.map(el => JSON.parse(el).adminAccess),
      }});

    });
  });
};
