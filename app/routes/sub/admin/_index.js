'use strict';

const express = require('express');
const container = require('../../../utils/container.js');

const router = express.Router()
  .use(container.get('sessRedis'))

  .post('/main/logIn', require('./logIn.js'))

  .use((req, res, next) => req.session.adminAccess ? next() : res.render('admin', {page:'admin/logIn'}))

  .get('/main', (req, res) => res.render('admin', {page:'admin/main'}))
  .post('/main/logIn', (req, res) => res.redirect('/'))
  .get('/main/logOut', (req, res) => {req.session.adminAccess = false; res.redirect('/');})

  .get('/main/session',   require('./redisSession.js'))
  .get('/main/support',   require('./redisSupport.js'))
  .get('/main/supportAll',require('./redisSupportAll.js'))
  .get('/main/listGET',   require('./redisQueryLogs.js')('GET'))
  .get('/main/listPOST',  require('./redisQueryLogs.js')('POST'))

  .get('/main/users', require('./mysqlEdit.js')('users'))
  .get('/main/goods', require('./mysqlEdit.js')('goods'))
  .get('/main/mails', require('./mysqlEdit.js')('mails'))
  .get('/main/transactions',  require('./mysqlEdit.js')('trans'))
  // .get('/main/adminActions',  require('./mysqlReport.js')('admin'))

  .all('*', (req, res) => res.render('error', {code:'404'}));



module.exports = router;




// 'use strict';
//
// const path = require('path');
// const express = require('express');
// // const router = express.Router();
// const hbs = require('express-handlebars');
//
//
// module.exports = (req, res, next, app) => {
//   app.engine('hbs', hbs.create({
//     partialsDir: path.join(__dirname, '../../../views/main/partials'),
//     extname: 'hbs',
//     helpers: {
//       'include': template => template.data.root['page']
//     }
//   }).engine);
//   app.set('views', path.join(__dirname, '../../../views/main'));
//
//   let router = express.Router()
//     .get('/main', (req, res) => res.render('index'))
//
//     .get('*', (req, res) => res.render('error', {code:'404'+ req.url}));
//
//   return router(req, res, next);
// };
