class Config {
  constructor() {
    this.ip = '127.0.0.1';
    this.port = '3000';
    this.baseUrl = process.env.VCAP_APPLICATION || 'localhost';

    this.rewrite = [
      ['/{2,}', '/', 'R'],
      ['(.*/)index\\.\\w+', '$1', 'R'],
      ['^(?<!/main)(.*)$', '/main$1', 'L'],
      ['^/(\\?.*)?$', '/main$1', 'L'],
    ];

    this.mail = {
      smtpRambler: {
        host: 'smtp.mySecretKey.ru',
        port: 465,
        auth: {
          user: 'mySecretKey.mail2018@mySecretKey.ru',
          pass: 'mySecretKey',
        }
      },
    };

    this.redis = {
      options: {
        host: '127.0.0.1',
        port: '6379',
          prefix: 'proShop:',
      },
      select: 0,
      tables: {
        queryLogs: 'queryLogs',
        // members: 'members',
        // ID_nick: 'ID_nick',
      }
    };

    this.session = {
      options: {
        secret: 'mySecretKey',
        path: '/',
        resave: false,
        saveUninitialized: true,
      },
      redis: {
        prefix: this.redis.options.prefix + 'session:',
        ttl: 12 * 3600,
      },
    };

    this.mysql = {
      options: {
        host: '127.0.0.1',
        port: '3306',
        user: 'proShop',
        password: 'mySecretKey',

        multipleStatements:true,
        // insecureAuth:true,
      },
      select: 'main',
      tables: {
        users: {
          name: 'users',
          fields: [
            'id INT primary key auto_increment',
            'name VARCHAR(99) NOT NULL',
            'pass VARCHAR(99) NOT NULL',
            'email VARCHAR(99) NOT NULL',
            'regDate TIMESTAMP NOT NULL',
          ]
        },
        bans: {
          name: 'bans',
          fields: [
            'id INT primary key auto_increment',
            'userID INT NOT NULL',
            'dateStart TIMESTAMP NOT NULL',
            'dateEnd TIMESTAMP NOT NULL',
            'reason VARCHAR(255) NOT NULL',
          ]
        },
        worker: {
          name: 'worker',
          fields: [
            'id INT primary key auto_increment',
            'userID INT NOT NULL',
            'roleID INT NOT NULL',
            'dateUpdate TIMESTAMP NOT NULL',
          ]
        },
        workerRoles: {
          name: 'workerRoles',
          fields: [
            'id INT primary key auto_increment',
            'name VARCHAR(99) NOT NULL',
            'text TEXT(999) NOT NULL',
          ]
        },
        goods: {
          name: 'goods',
          fields: [
            'id INT primary key auto_increment',
            'type VARCHAR(99) NOT NULL',
            'name VARCHAR(99) NOT NULL',
            'price INT NOT NULL',
            'amount INT NOT NULL',
            'image VARCHAR(255) NOT NULL',
            'feature TEXT(999) NOT NULL',
          ]
        },
        trans: {
          name: 'transactions',
          fields: [
            'id INT primary key auto_increment',
            'userID INT NOT NULL',
            'goodID INT NOT NULL',
            'date TIMESTAMP NOT NULL',
          ]
        },
        mails: {
          name: 'templateMails',
          fields: [
            'id INT primary key auto_increment',
            'title VARCHAR(99) NOT NULL',
            'text TEXT(999) NOT NULL',
          ]
        },

      }
    };

  }
}



module.exports = Config;
