'use strict';

const fs = require('fs');
const path = require('path');

const container = require('../utils/container.js');


module.exports = (req, res, next, app) => {
  let config = container.get('config');
  let logger = container.get('logger');
  let log = (...args) => this.logger.log(module, args);

  let sub = req.hostname.split(config.baseUrl).filter(el => el)[0];
  if (!sub || sub.startsWith('_')) return next();

  let file = path.resolve(__dirname, '../routes/sub/', sub.slice(0, -1), '_index.js');
  fs.exists(file, exists => exists ? require(file)(req, res, next, app) : next({code:404, message:'Субдомен не существует'}));
}
