'use strict';


const container = require('../utils/container.js');


module.exports = function(req, res, next) {
  let config = container.get('config');
  let logger = container.get('logger');
  let log = (...args) => this.logger.log(module, args);

  let redis = container.get('redis');

  redis.hset(config.redis.tables.queryLogs +':'+ req.method, Date.now(), JSON.stringify({ip:req.ip, url:req.protocol+'://'+req.hostname+req.url}), (err, reply) => err ? this.log(err, 7) : next());
};
