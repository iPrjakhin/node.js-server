document.addEventListener('DOMContentLoaded', () => {
  let modal = document.getElementById('modal');
  let header = modal.getElementsByClassName('uk-modal-header')[0];
  let form = modal.getElementsByTagName('form')[0];

  document.querySelectorAll('tr').forEach(tr => tr.addEventListener('click', () =>
    modalRender(document.querySelector('table[nameID]').getAttribute('nameID'), tr.getAttribute('data'))
  ));

  function modalRender(table, opt) {
    if (typeof opt == 'string') opt = JSON.parse(opt);
    let formData = '';


    if (table == 'users') {
      header.innerHTML = `<h2 class="uk-modal-title">${opt.name}</h2>${opt.email}`;
      formData = `
        <div class="uk-margin">
          <label class="uk-form-label" for="mfUsersName">Изменить имя</label>
          <div class="uk-form-controls">
            <input class="uk-input" id="mfUsersName" name="mfUsersName" type="text" placeholder="Имя" value="${opt.name}">
          </div>
        </div>

        <div class="uk-margin">
          <label class="uk-form-label" for="mfUsersEmail">Изменить E-mail</label>
          <div class="uk-form-controls">
            <input class="uk-input" id="mfUsersEmail" name="mfUsersEmail" type="text" placeholder="E-mail" value="${opt.email}">
          </div>
        </div>

        <div class="uk-margin">
          <label class="uk-form-label" for="mfUsersPass">Изменить пароль</label>
          <div class="uk-form-controls">
            <input class="uk-input" id="mfUsersPass" name="mfUsersPass" type="password" placeholder="Пароль" value="">
          </div>
        </div>

        <div class="uk-margin">
          <label class="uk-form-label" for="mfUsersBan">Заблокировать пользователя</label>
          <div class="uk-form-controls">
            <input class="uk-input" id="mfUsersBan" name="mfUsersBan" type="text" placeholder="Причина" value="">
          </div>
        </div>
      `;
    }

    if (table == 'goods') {
      header.innerHTML = `<h2 class="uk-modal-title">${opt.name}</h2>${opt.type}`;
      formData = `
        <div class="uk-margin">
          <label class="uk-form-label" for="mfGoodsType">Изменить группу товара</label>
          <div class="uk-form-controls">
            <select class="uk-select" id="mfGoodsType" name="mfGoodsType">
              <option>${opt.type}</option>
            </select>
          </div>
        </div>

        <div class="uk-margin">
          <label class="uk-form-label" for="mfGoodsName">Изменить название</label>
          <div class="uk-form-controls">
            <input class="uk-input" id="mfGoodsName" name="mfGoodsName" type="text" placeholder="Название" value="${opt.name}">
          </div>
        </div>

        <div class="uk-margin">
          <label class="uk-form-label" for="mfGoodsPrice">Изменить цену</label>
          <div class="uk-form-controls">
            <input class="uk-input" id="mfGoodsPrice" name="mfGoodsPrice" type="number" placeholder="Цена" value="${opt.price}">
          </div>
        </div>

        <div class="uk-margin">
          <label class="uk-form-label" for="mfGoodsAmount">Изменить количество</label>
          <div class="uk-form-controls">
            <input class="uk-input" id="mfGoodsAmount" name="mfGoodsAmount" type="number" placeholder="Количество" value="${opt.amount}">
          </div>
        </div>

        <div class="uk-margin">
          <textarea class="uk-textarea" rows="4" name="mfMailsText" placeholder="Изменить тело письма">${opt.feature}</textarea>
        </div>
      `;
    }

    if (table == 'mails') {
      header.innerHTML = `<h2 class="uk-modal-title">Письмо</h2>${opt.title}`;
      formData = `
        <div class="uk-margin">
          <label class="uk-form-label" for="mfMailsTitle">Изменить заголовок</label>
          <div class="uk-form-controls">
            <input class="uk-input" id="mfMailsTitle" name="mfMailsTitle" type="text" placeholder="Заголовок" value="${opt.title}">
          </div>
        </div>

        <div class="uk-margin">
          <textarea class="uk-textarea" rows="4" name="mfMailsText" placeholder="Изменить тело письма">${opt.text}</textarea>
        </div>
      `;
    }


    form.innerHTML = formData;
    UIkit.modal('#modal').show();
  }

});
