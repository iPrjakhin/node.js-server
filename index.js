/* Автор - Пряхин Игорь  ::  BART96  ::  Author - Prjakhin Igor */
/* Уважайте чужой труд.  ::  Respect other peoples work. */

'use strict';

if (Number(process.version.slice(1).split('.')[0]) < 9) throw new Error('Node 9 or higher is required. Update Node on your system.');


// const domain = require('domain').create();
//
// domain.on('error', error => console.log(`\x1b[41m> Выловлена ошибка:\x1b[0m ${error.name} \x1b[90m=>\x1b[39m ${error.message}\n ${error.stack.split(/\n/).slice(0, 3).join('\n')}`));
// domain.run(() => require('./app/App.js'));

let App = require('./app/app.js');
let app = new App();
