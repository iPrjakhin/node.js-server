-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Июн 25 2018 г., 18:56
-- Версия сервера: 5.5.60
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `main`
--

-- --------------------------------------------------------

--
-- Структура таблицы `bans`
--

CREATE TABLE `bans` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `dateStart` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateEnd` datetime NOT NULL,
  `reason` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `bans`
--

INSERT INTO `bans` (`id`, `userID`, `dateStart`, `dateEnd`, `reason`) VALUES
(5, 1, '2018-06-01 20:04:24', '2018-06-01 23:00:00', 'Бан будет до 06.06.2018'),
(6, 3, '2017-12-31 21:00:00', '2018-01-03 00:00:00', 'Закончился бан'),
(7, 7, '2018-06-01 20:03:31', '0000-00-00 00:00:00', 'Вечный бан'),
(8, 9, '2018-06-04 21:00:00', '2018-06-22 00:00:00', '');

-- --------------------------------------------------------

--
-- Структура таблицы `goods`
--

CREATE TABLE `goods` (
  `id` int(11) NOT NULL,
  `type` varchar(99) NOT NULL,
  `name` varchar(99) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `feature` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `goods`
--

INSERT INTO `goods` (`id`, `type`, `name`, `price`, `amount`, `image`, `feature`) VALUES
(1, 'Прессовое оборудование', 'Orwak 3320', 225398, 32, '12', 'надежная конструкция, низкий уровень шума, переключатель типа материала'),
(2, 'Шредер', 'RSwak 3420-Combi', 385437, 30, '15', 'рабочий цикл 24 секунды, функция автостарт, низкий уровень шума, переключатель типа материала'),
(3, 'Компактор', 'Мобильные компакторы MPC', 266395, 45, '1', 'плотность уплотнения (330 кН), объем уплотнения (2,2 м3/цикл),  могут быть оснащены автоматической функцией оповещения'),
(4, 'Шредер', 'RSwak 3620', 285922, 34, '16', 'надежная конструкция, низкий уровень шума, удобная панель управления, переключатель типа материала'),
(5, 'Прессовое оборудование', 'Orwak 3110S', 124854, 15, '8', 'регулируемый размер кипы, низкий уровень шума, переключатель типа материала'),
(6, 'Шредер', 'RSwak 3420', 208666, 36, '14', 'рабочий цикл 24 секунды, функция автостарт, низкий уровень шума, переключатель типа материала'),
(7, 'Компактор', 'Био-Компакторы SKPC', 422950, 41, '4', 'уплотнения влажных отходов, самоочищающаяся пресс плита, большой объем загрузки'),
(8, 'Прессовое оборудование', 'Orwak 3325', 332538, 43, '13', 'функция автостарт, низкий уровень шума, переключатель типа материала, индикатор готовности'),
(9, 'Прессовое оборудование', 'Пресс TOM', 294976, 26, '7', 'датчик движения, индикатор заполнения, автоматическое оповещение'),
(10, 'Прессовое оборудование', 'Orwak 3115', 239771, 31, '9', 'поперечная обвязка кипы, регулируемый размер кипы, низкий уровень шума, переключатель типа материала'),
(11, 'Шредер', 'RSwak 3820', 108931, 23, '17', 'специальный режим для платсика, широкое загрузочное окно'),
(12, 'Компактор', 'Стационарные компакторы STP', 399257, 30, '2', 'большая производительность, большая сила уплотнения, средства автоматического крепления'),
(13, 'Компактор', 'Стационарные компакторы STP-K', 223656, 33, '3', 'большая производительность, сила уплотнения (320 кН), средства автоматической фиксации'),
(14, 'Компактор', 'Индустриальные компакторы STP', 345990, 22, '6', 'сила прессования (до 600кН), очень прочная конструкция, высокая производительность'),
(15, 'Прессовое оборудование', 'Orwak 3210', 411739, 32, '10', 'рабочий цикл 13 секунд, низкий уровень шума, переключатель типа материала'),
(16, 'Прессовое оборудование', 'Orwak 3210-Combi', 145996, 41, '11', 'прессование бочек,индикатор готовности, переключатель типа мтериала'),
(17, 'Компактор', 'Шнековые мобильные компакторы MSV', 168697, 13, '5', 'рабочий цикл 24 секунды, функция автостарт, низкий уровень шума, переключатель типа материала');

-- --------------------------------------------------------

--
-- Структура таблицы `templatemails`
--

CREATE TABLE `templatemails` (
  `id` int(11) NOT NULL,
  `title` varchar(99) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `templatemails`
--

INSERT INTO `templatemails` (`id`, `title`, `text`) VALUES
(1, 'Регистрация', '<h3>Hello</h3>Спасибо за регистрацию на <a href=\"http://proShop.ru\" target=\"_blank\">proShop.ru</a>\r\n\r\n_________\r\nProShop - Ваш профессиональный партнер');

-- --------------------------------------------------------

--
-- Структура таблицы `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `goodID` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transactions`
--

INSERT INTO `transactions` (`id`, `userID`, `goodID`, `date`) VALUES
(1, 1, 5, '2018-06-01 00:10:11'),
(2, 3, 5, '2018-06-02 00:05:53'),
(5, 2, 9, '2018-06-02 00:07:11'),
(6, 3, 13, '2018-06-02 00:07:11');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(99) NOT NULL,
  `pass` varchar(99) NOT NULL,
  `email` varchar(99) NOT NULL,
  `regDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `pass`, `email`, `regDate`) VALUES
(1, 'user1', '123123', 'user1@mail.mail', '2018-06-06 12:13:04'),
(2, 'user2', '123123', 'user2@mail.mail', '2018-06-08 12:13:04'),
(3, 'user3', '123123', 'user3@mail.mail', '2018-06-10 12:13:04'),
(4, 'user4', '123123', 'user4@mail.mail', '2018-06-12 12:13:04'),
(5, 'user5', '123123', 'user5@mail.mail', '2018-06-14 12:13:04'),
(6, 'user6', '123123', 'user6@mail.mail', '2018-06-16 12:13:04'),
(7, 'user7', '123123', 'user7@mail.mail', '2018-06-17 12:13:04'),
(8, 'user8', '123123', 'user8@mail.mail', '2018-06-19 12:13:04'),
(9, 'user9', '123123', 'user9@mail.mail', '2018-06-21 12:13:04'),
(10, 'user10', '123123', 'user10@mail.mail', '2018-06-23 12:13:04'),
(11, 'user11', '123123', 'user11@mail.mail', '2018-06-24 12:13:04'),
(12, 'user12', '123123', 'user12@mail.mail', '2018-06-26 12:13:04'),
(13, 'user13', '123123', 'user13@mail.mail', '2018-06-28 12:13:04'),
(14, 'user14', '123123', 'user14@mail.mail', '2018-06-29 12:13:04'),
(15, 'user15', '123123', 'user15@mail.mail', '2018-06-30 12:13:04'),
(16, 'user16', '123123', 'user16@mail.mail', '2018-06-01 06:13:04'),
(17, 'user17', '123123', 'user17@mail.mail', '2018-06-01 10:13:04'),
(18, 'user18', '123123', 'user18@mail.mail', '2018-06-01 07:13:04'),
(19, 'user19', '123123', 'user19@mail.mail', '2018-06-01 12:24:04'),
(20, 'user20', '123123', 'user20@mail.mail', '2018-06-01 12:41:04');

-- --------------------------------------------------------

--
-- Структура таблицы `worker`
--

CREATE TABLE `worker` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `roleID` int(11) NOT NULL,
  `dateUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `workerroles`
--

CREATE TABLE `workerroles` (
  `id` int(11) NOT NULL,
  `name` varchar(99) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `workerroles`
--

INSERT INTO `workerroles` (`id`, `name`, `text`) VALUES
(1, 'Рук.вод', 'Руководить отдела'),
(2, 'Зам.рук.вода', 'Заместитель руководителя отдела'),
(3, 'Менеджер (1)', 'Связь с поставщиками'),
(4, 'Менеджер (2)', 'Связь со складом'),
(5, 'Инженер', '');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `templatemails`
--
ALTER TABLE `templatemails`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `worker`
--
ALTER TABLE `worker`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `workerroles`
--
ALTER TABLE `workerroles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `bans`
--
ALTER TABLE `bans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `goods`
--
ALTER TABLE `goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `templatemails`
--
ALTER TABLE `templatemails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `worker`
--
ALTER TABLE `worker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `workerroles`
--
ALTER TABLE `workerroles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
